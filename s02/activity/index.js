// Quiz

// 1. Spaghetti Code
// 2. let OBJECT_NAME = {}
// 3. Object Oriented Programming
// 4. studentOne.enroll();
// 5. True
// 6. KEY: "VALUE"
// 7. True
// 8. True
// 9. True
// 10. True


// Function Coding	

let studentOne = {
    name: "John",
    email: "john@mail.com",
    grades: [89, 84, 78, 88],

    gradeAverage(){
    	return this.average = this.grades.reduce((total, item) => {return total+=item}) / 4
    },
    willPass(){
    	return this.average >= 85
    },
    willPassWithHonors(){
    	return this.average >= 90 ? true : this.average >= 85 && this.average < 90 ? false : undefined;
    }
}

let studentTwo = {
    name: "Joe",
    email: "joe@mail.com",
    grades: [78, 82, 79, 85],
    gradeAverage(){
    	return this.average = this.grades.reduce((total, item) => {return total+=item}) / 4
    },
    willPass(){
    	return this.average >= 85
    },
    willPassWithHonors(){
    	return this.average >= 90 ? true : this.average >= 85 && this.average < 90 ? false : undefined;
    }
}

let studentThree = {
	name: "Jane",
    email: "jane@mail.com",
    grades: [87, 89, 91, 93],

    gradeAverage(){
    	return this.average = this.grades.reduce((total, item) => {return total+=item}) / 4
    },
    willPass(){
    	return this.average >= 85
    },
    willPassWithHonors(){
    	return this.average >= 90 ? true : this.average >= 85 && this.average < 90 ? false : undefined;
    }
}

let studentFour = {
	name: "Jessie",
    email: "jessie@mail.com",
    grades: [91, 89, 92, 93],

    gradeAverage(){
    	return this.average = this.grades.reduce((total, item) => {return total+=item}) / 4
    },
    willPass(){
    	return this.average >= 85
    },
    willPassWithHonors(){
    	return this.average >= 90 ? true : this.average >= 85 && this.average < 90 ? false : undefined;
    }
} 

let classOf1A = {
	students: [studentOne, studentTwo, studentThree, studentFour],
	countHonorStudents(){
		return this.honorStudentsCount = this.students.filter((student) => { return student.gradeAverage() >= 90 }).length
	},
	honorsPercentage(){
		return (this.honorStudentsCount / this.students.length) * 100
	},
	retrieveHonorStudentInfo(){
		this.array = []
		this.students.filter((student) => { 
			if(student.gradeAverage() >= 90){
				this.array.push({email: student.email, average: student.gradeAverage()})
			}
		})
		return this.array
	},
	sortHonorStudentsByGradeDesc(){
		return this.array.sort((studentA, studentB) => {return studentB.average - studentA.average})
	}
}