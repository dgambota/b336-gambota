class Section {
    constructor(name) {
        this.name = name;
        this.students = [];
        this.honorStudents = undefined;
        this.honorsPercentage = undefined;
    }  

    addStudent(name, email, grades){
        this.students.push(new Student(name, email, grades))
        return this
    }

    countHonorStudents(){
        this.honorStudents = this.students.filter((student) => {
            return student.computeAve().willPass().willPassWithHonors().passed == true
        }).length
        return this
    }

    computeHonorsPercentage(){
        this.honorsPercentage = (this.honorStudents / this.students.length) * 100
        return this
    }
}

class Student {
    constructor(name, email, grades){
        this.name = name;
        this.email = email;
        if(grades.length === 4){
            if(grades.every(grade => grade >= 0 && grade <= 100)){
                this.grades = grades;
            } else {
                this.grades = undefined;
            }
        } else {
            this.grades = undefined;
        }
        this.gradeAve = undefined;
        this.passed = undefined;
        this.passedWithHonors = undefined;
    }

    login(){
        console.log(`${this.email} has logged in`);
        return this;
    }
    logout(){
        console.log(`${this.email} has logged out`);
        return this;
    }
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
        return this;
    }
    computeAve(){
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        this.gradeAve = sum/4;
        return this;
    }
    willPass() {
        this.passed = this.computeAve().gradeAve >= 85 ? true : false;
        return this;
    }
    willPassWithHonors() {
        if (this.passed) {
            if (this.gradeAve >= 90) {
                this.passedWithHonors = true;
            } else {
                this.passedWithHonors = false;
            }
        } else {
            this.passedWithHonors = false;
        }
        return this;
    }
}

class Grade{
	constructor(grade){
		this.level = grade;
		this.sections = [];
		this.totalStudents = 0;
		this.totalHonorStudents = 0;
		this.batchAveGrade = undefined;
		this.batchMinGrade = undefined;
		this.batchMaxGrade = undefined;
	}

	addSection(sectionName){
		this.sections.push(new Section(sectionName))
		return this
	}

	countStudents(){
		let total = 0;
		this.sections.map((section) => {
			total += section.students.length
		})
		this.totalStudents = total
		return this
	}
	countHonorStudents(){
		let total = 0;
		this.sections.map((section) => {
			section.students.map((student) => {
				if(student.willPass().computeAve().willPassWithHonors().passedWithHonors){
					total++
				}
			})
		})
		this.totalHonorStudents = total
		return this
	}
	computeBatchAve(){
		let total = 0;
		this.sections.map((section) => {
			section.students.map((student) => {
				total += student.computeAve().gradeAve
			})
		})
		this.batchAveGrade = total / this.totalStudents;
		return this
	}
	getBatchMinGrade(){
		let allGrades = []
		this.sections.map((section) => {
			section.students.map((student) => {
				student.grades.map((grade) =>{
					allGrades.push(grade)
				})
			})
		})
		this.batchMinGrade = Math.min(...allGrades)
		return this
	}
	getBatchMaxGrade(){
		let allGrades = []
		this.sections.map((section) => {
			section.students.map((student) => {
				student.grades.map((grade) =>{
					allGrades.push(grade)
				})
			})
		})
		this.batchMaxGrade = Math.max(...allGrades)
		return this
	}
}

let grade1 = new Grade(1)


grade1.addSection('section1A')
	  .addSection('section1B')
	  .addSection('section1C')
	  .addSection('section1D')

grade1.sections.find(section => section.name == 'section1A')
	.addStudent('John', 'john@mail.com', [89, 84, 78, 88])
	.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85])
	.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93])
	.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

grade1.sections.find(section => section.name == 'section1B')
	.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89])
	.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88])
	.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91])
	.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

grade1.sections.find(section => section.name == 'section1C')
	.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91])
	.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89])
	.addStudent('Love', 'love@mail.com', [91, 87, 90, 88])
	.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

grade1.sections.find(section => section.name == 'section1D')
	.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92])
	.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90])
	.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86])
	.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);