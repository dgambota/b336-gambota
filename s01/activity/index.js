// Quiz

// 1. let ARRAY_NAME = [];
// 2. ARRAY_NAME[0];
// 3. ARRAY_NAME[ARRAY_NAME.length - 1]
// 4. ARRAY_NAME.indexOf();
// 5. ARRAY_NAME.forEach();
// 6. ARRAY_NAME.map();
// 7. ARRAY_NAME.every()
// 8. ARRAY_NAME.some()
// 9. False
// 10. True

// Function Coding


function addToEnd(array, element){
	if(typeof element == 'string'){
		array.push(element);
		return array
	}
	return 'error - can only add strings to an array';
}	

function addToStart(array, element){
	if(typeof element == 'string'){
		array.unshift(element);
		return array
	}
	console.log('error - can only add strings to an array');
}

function elementChecker(array, element){
	if(array.length == 0){
		return 'error - passed in array is empty'
	}
	return array.includes(element);
}

function checkAllStringsEnding(array, element){
	if(array.length == 0){
		return 'error - array must NOT be empty'
	}
	if(array.some((item) => {return typeof item != 'string'})){
		return 'error - all array elements must be strings'
	}
	if(typeof element != 'string'){
		return 'error - 2nd argument must be of data type string'
	}
	if(element.length > 1){
		return 'error - 2nd argument must be a single character'
	}

	return array.every((item) => {
		return item[item.length - 1] == element;
	})
}

function stringLengthSorter(array){
	if(array.some((item) => {return typeof item != 'string'})){
		return 'error - all array elements must be strings'
	}

	return array.sort((itemA, itemB) => {
		return itemA.length - itemB.length;
	})
}

function startsWithCounter(array, char){
	if(array.length == 0){
		return 'error - array must NOT be empty'
	}
	if(array.some((item) => {return typeof item != 'string'})){
		return 'error - all array elements must be strings'
	}
	if(typeof char != 'string'){
		return 'error - 2nd argument must be of data type string'
	}
	if(char.length > 1){
		return 'error - 2nd argument must be a single character'
	}

	return array.filter((item) => {
		return item[0].toLowerCase() == char.toLowerCase();
	}).length;
}

function likeFinder(array, str){
	if(array.length == 0){
		return 'error - array must NOT be empty'
	}
	if(array.some((item) => {return typeof item != 'string'})){
		return 'error - all array elements must be strings'
	}
	if(typeof str != 'string'){
		return 'error - 2nd argument must be of data type string'
	}

	return array.filter((item) => {
		return item.toLowerCase().includes(str.toLowerCase());
	})
}

function randomPicker(array){
	return array[Math.floor(Math.random() * ((array.length - 1)- 0) ) + 0]
}