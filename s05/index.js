class Customer{
	constructor(email, cart){
		this.email = email;
		this.cart = new Cart();
		this.orders = [];
	}

	checkOut(){
		if(this.cart.length !== 0){
			this.orders.push({products: this.cart.contents, totalAmount: this.cart.totalAmount})
		}
		return this
	}
}

class Cart{
	constructor(){
		this.contents = []
		this.totalAmount = 0;
	}
	addToCart(product, quantity){	
		this.contents.push({products: product, quantity })
		return this
	}
	showCartContents(){
		return this.contents
	}
	updateProductQuantity(product, newQuantity){
		this.contents.map((content) => {
			content.product.name == product ? content.quantity = newQuantity : false
		})
		return this
	}
	clearCartContents(){
		this.contents.length = 0;
		return this
	}
	computeTotal(){
		this.totalAmount = 0;
		this.contents.map((content) => {
			this.totalAmount += content.products.price * content.quantity;
		})
		return this
	}
}

class Product{
	constructor(name, price){
		this.name = name;
		this.price = price;
		this.isActive = true;
	}

	archive(){
		this.isActive = false;
		return this
	}
	updatePrice(newPrice){
		this.price = newPrice;
		return this
	}
}


// const john = new Customer('john@mail.com')
// const prodA = new Product('soap', 9.99)
// prodA.updatePrice(12.99)
// prodA.archive()
// john.cart.addToCart(prodA, 3)
// john.cart.showCartContents()
// john.cart.updateProductQuantity('soap', 5)
// john.cart.clearCartContents()
// john.cart.computeTotal()
// john.checkOut()