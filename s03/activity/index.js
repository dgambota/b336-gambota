// Quiz

// 1. No
// 2. No
// 3. Yes
// 4. getter and setter
// 5. Object


// Function Coding
class Student{
	constructor(name, email, grades){
		this.name = name;
		this.email = email;
		if(grades.length === 4){
			if(grades.every(grade => {return grade >= 0 && grade <= 100})){
				this.grades = grades
			}
		} else{
			this.grades = undefined;
		}

		this.gradeAve = undefined;
		this.isPass = undefined;
		this.isWithHonors = undefined;
	}
	login() {
	    console.log(`${this.email} has logged in`);
	    return this
	}

	logout() {
	    console.log(`${this.email} has logged out`);
	    return this
	}

	listGrades() {
	    console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
	    return this
	}
	computeAve() {
	    let sum = 0;
	    this.grades.forEach(grade => sum = sum + grade);
	    // return sum/4;
	    this.gradeAve = sum / 4;
	    return this;
	}

	willPass() {
	    this.isPass = this.computeAve() >= 85 ? true : false;
	    return this;
	}

	willPassWithHonors() {
	    this.isWithHonors = (this.willPass() && this.computeAve() >= 90) ? true : false;
	    return this;
	}


}

let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);